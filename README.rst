djinfo
======

.. image:: https://gitlab.com/h3/djinfo/badges/master/build.svg
    :target: https://gitlab.com/h3/djinfo/pipelines

.. image:: https://readthedocs.org/projects/djinfo/badge/?version=latest
    :target: https://djinfo.readthedocs.io/en/latest/

.. image:: https://badge.fury.io/py/djinfo.svg
    :target: https://pypi.org/project/djinfo/

.. .. image:: https://pypip.in/d/djinfo/badge.png

Django debugging information page suitable for production.

.. figure:: docs/topics/img/djinfo-screenshot.png
    :alt: Screenshot
    :align: center
    :width: 1003px


Supported Python/Django versions
--------------------------------

* django: **1.8, 1.9, 1.10, 1.11, 2.0, 2.1**
* python: **2.7, 3.4, 3.5, 3.6, 3.7**

Links
-----

* `djinfo documentation <https://djinfo.readthedocs.io/en/latest/>`_
