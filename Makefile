.PHONY: all lint test build release release-test clean docs help

all: help

clean:
	rm -rf build/ dist/ docs/_build/

test:
	detox

lint:
	tox -e linters

build:
	rm -rf build/*
	tox -e build

docs:
	rm -rf docs/_build/
	tox -e docs

release:
	@make lint
	@make build
	rm -rf dist/*
	tox -e release

release-test:
	@make lint
	@make build
	rm -rf dist/*
	tox -e release-test

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  clean         to remove all build files"
	@echo "  lint          to run linters"
	@echo "  test          to run tests"
	@echo "  build         to build python package"
	@echo "  docs          to build documentation"
	@echo "  release       to create a new release on pypi.org"
	@echo "  release-test  to create a release on test.pypi.org"
