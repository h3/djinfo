# -*- coding: utf-8 -*-
"""
djinfo view tests
"""

import json
from django.conf import settings
from djinfo.tests.utils import TestCase


class DjinfoViewTestCase(TestCase):
    """ djinfo view tests """

    def test_view_superuser(self):
        with self.login(username='superuser'):
            self.get_check_200('djinfo:index')

    def test_view_manager(self):
        with self.login(username='manager'):
            self.response_302(self.get('djinfo:index'))

    def test_view_user(self):
        with self.login(username='user'):
            self.response_302(self.get('djinfo:index'))

    def test_view_anonymous(self):
        self.assertLoginRequired('djinfo:index')

    def test_json_view_superuser(self):
        with self.login(username='superuser'):
            rs = self.get('djinfo:index', data={'json': True})
        try:
            data = json.loads(str(rs.content, encoding='utf8'))
        except TypeError:  # python 2.7
            data = json.loads(str(rs.content))
        except Exception as e:
            data = {}
        self.assertEqual(data.get('settings', {}).get('SITE_ID'), 1)

    def test_json_view_manager(self):
        with self.login(username='manager'):
            rs = self.get('djinfo:index', data={'json': True})
            self.response_302(rs)

    def test_json_view_user(self):
        with self.login(username='user'):
            rs = self.get(self.reverse('djinfo:index'), data={'json': True})
            self.response_302(rs)

    def test_json_view_anonymous(self):
        rs = self.get(self.reverse('djinfo:index'), data={'json': True})
        self.response_302(rs)
