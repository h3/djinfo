# -*- coding: utf-8 -*-
"""
djinfo generic utils
"""
from __future__ import unicode_literals
from test_plus.test import TestCase as PlusTestCase


class TestCase(PlusTestCase):

    def setUp(self):
        self.superuser = self.create_user(
            'superuser', superuser=True, staff=True)
        self.manager = self.create_user('manager', superuser=False, staff=True)
        self.user = self.create_user('user', superuser=False, staff=False)

    def create_user(self, username, staff=False, superuser=False):
        user = self.make_user(username)
        if staff:
            user.is_staff = True
        if superuser:
            user.is_superuser = True
        if staff or superuser:
            user.save()
        return user

    def getJson(self):
        with self.login(username='superuser'):
            rs = self.get('djinfo:index', data={'json': True})
        try:
            data = json.loads(str(rs.content, encoding='utf8'))
        except TypeError:  # python 2.7
            data = json.loads(str(rs.content))
        except Exception as e:
            data = {}
        return rs, data
