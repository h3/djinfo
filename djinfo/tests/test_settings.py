# -*- coding: utf-8 -*-
"""
djinfo settings tests
"""

import json
from django.test import override_settings
from djinfo.utils import MASK
from djinfo.tests.utils import TestCase


class DjinfoSettingsTestCase(TestCase):
    """ djinfo settings tests """

    def test_default_masks(self):
        with self.login(username='superuser'):
            rs = self.get('djinfo:index', data={'json': True})
        try:
            data = json.loads(str(rs.content, encoding='utf8'))
        except TypeError:  # python 2.7
            data = json.loads(str(rs.content))
        except Exception as e:
            data = {}
        self.assertEqual(
            data.get('settings', {}).get('SECRET_KEY'), MASK)

#   @override_settings(DJINFO_SHOW=['settings'])
#   def test_hide_environment(self):
#       rs, data = self.getJson()
#       self.assertEqual(data.get('environment', {}), {})

#   @override_settings(DJINFO_SHOW=['environment'])
#   def test_hide_settings(self):
#       rs, data = self.getJson()
#       self.assertEqual(data.get('settings', {}), {})

#   @override_settings(DJINFO_SHOW=[])
#   def test_show_nothing(self):
#       rs, data = self.getJson()
#       self.assertEqual(data.get('settings', {}), {})
#       self.assertEqual(data.get('environment', {}), {})
