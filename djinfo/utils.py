# -*- coding: utf-8 -*-
"""
djinfo generic utils
"""
from __future__ import unicode_literals

import os
import re
import copy
import logging

from datetime import timedelta
from collections import OrderedDict

from django import __version__
from django.conf import settings

from djinfo.settings import SETTINGS_MASKS, ENV_MASKS, ENV_EXCLUDE, SHOW

logger = logging.getLogger(__name__)

MASK = '*' * 15


def is_masked(k, masks):
    """
    Returns True if provided value should be masked for provided key
    """
    masked = False
    for m in masks:
        if re.search(m, k):
            masked = True
    return masked


def is_excluded(k, excludes):
    """
    Returns True if provided value should be excluded for provided key
    """
    excluded = False
    for e in excludes:
        if re.search(e, k):
            excluded = True
    return excluded


def masked_dict(d):
    """
    Recursively mask a dictionary
    """
    o = {}
    for k, v in d.items():
        if isinstance(v, dict):
            o[k] = masked_dict(v)
        else:
            if isinstance(k, str) and is_masked(k, SETTINGS_MASKS):
                o[k] = MASK
            else:
                o[k] = v
    return o


def settings_as_dict():
    """
    Returns django settings as a representable dictionary, filtering
    and masking values
    """
    o = {}
    _settings = copy.deepcopy(settings.__dict__['_wrapped'].__dict__)
    for k, v in _settings.items():
        if is_masked(k, SETTINGS_MASKS):
            o[k] = MASK
        elif not k.startswith('_'):
            if isinstance(v, timedelta):
                o[k] = str(v)
            elif isinstance(v, (set, tuple)):
                o[k] = list(v)
            elif isinstance(v, dict):
                o[k] = masked_dict(v)
            else:
                o[k] = v
    return OrderedDict(sorted(o.items(), key=lambda t: t[0]))


def env_as_dict():
    """
    Returns the env as a dict, filtering and masking values
    """
    o = {}
    for k, v in os.environ.items():
        if not is_excluded(k, ENV_EXCLUDE):
            if is_masked(k, ENV_MASKS):
                o[k] = MASK
            else:
                o[k] = v
    return OrderedDict(sorted(o.items(), key=lambda t: t[0]))


def get_context():
    """
    Returns djinfo template context as dictionary
    """
    return {
        'settings': settings_as_dict() if 'settings' in SHOW else None,
        'environment': env_as_dict() if 'environment' in SHOW else None,
        'django': {
            'version': __version__,
        }
    }
