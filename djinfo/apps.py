"""
djinfo app
"""
from __future__ import unicode_literals

from django.apps import AppConfig


class DjinfoConfig(AppConfig):
    """
    djinfo app config
    """
    name = 'djinfo'
